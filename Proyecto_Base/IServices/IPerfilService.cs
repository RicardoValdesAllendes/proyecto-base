﻿using DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IServices
{
    public interface IPerfilService
    {
        IList<PerfilDTO> GetAll();
        PerfilDTO GetById(int id);
        bool Create(PerfilDTO product);
        bool Update(PerfilDTO product);
        bool Delete(int id);
    }
}
