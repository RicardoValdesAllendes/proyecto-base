﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTOS;

namespace IServices
{
    public interface IUsuarioService
    {
        IList<UsuarioDTO> GetAll();
        UsuarioDTO GetById(int id);
        UsuarioDTO UserLogin(string userName, string password);
        bool Create(UsuarioDTO product);
        bool Update(UsuarioDTO product);
    }
}
