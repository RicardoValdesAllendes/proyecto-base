﻿using NHibernate;
using NHibernate.Linq;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private UnitOfWork _unitOfWork;
        public Repository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = (UnitOfWork)unitOfWork;
        }
        protected ISession _session { get { return _unitOfWork.Session; } }

        public bool Create(T entity)
        {
            _unitOfWork.BeginTransaction();
            var ret = _session.Save(entity);
            _unitOfWork.Commit();
            return true;
        }

        public bool Create(System.Collections.Generic.IEnumerable<T> items)
        {
            _unitOfWork.BeginTransaction();
            foreach (T item in items)
            {
                _session.Save(item);
            }
            _unitOfWork.Commit();
            return true;
        }

        public bool Update(T entity)
        {
            _unitOfWork.BeginTransaction();
            _session.Update(entity);
            _unitOfWork.Commit();
            return true;
        }

        public bool Update(System.Collections.Generic.IEnumerable<T> items)
        {
            _unitOfWork.BeginTransaction();
            foreach (T item in items)
            {
                _session.Update(item);
            }
            _unitOfWork.Commit();
            return true;
        }

        public bool Delete(T entity)
        {
            _unitOfWork.BeginTransaction();
            _session.Delete(entity);
            _unitOfWork.Commit();
            return true;
        }

        public bool Save(T entity)
        {
            _unitOfWork.BeginTransaction();
            _session.SaveOrUpdate(entity);
            _unitOfWork.Commit();
            return true;
        }

        public bool Save(System.Collections.Generic.IEnumerable<T> items)
        {
            _unitOfWork.BeginTransaction();
            foreach (T item in items)
            {
                _session.SaveOrUpdate(item);
            }
            _unitOfWork.Commit();
            return true;
        }

        public bool Delete(System.Collections.Generic.IEnumerable<T> entities)
        {
            _unitOfWork.BeginTransaction();
            foreach (T entity in entities)
            {
                _session.Delete(entity);
            }
            _unitOfWork.Commit();
            return true;
        }

        public IQueryable<T> GetAll()
        {
            return _session.Query<T>();
        }

        public T GetById(int id)
        {
            return _session.Get<T>(id);
        }

        public IQueryable<T> FilterBy(System.Linq.Expressions.Expression<System.Func<T, bool>> expression)
        {
            return GetAll().Where(expression).AsQueryable();
        }

    }
}
