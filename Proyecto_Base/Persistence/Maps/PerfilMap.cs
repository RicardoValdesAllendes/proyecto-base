﻿using DTOS;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Maps
{
    public class PerfilMap : ClassMap<PerfilDTO>
    {
        public PerfilMap()
        {
            Schema("\"public\"");
            Table("\"Perfil\"");

            Id(x => x.Id).Column("id").GeneratedBy.Identity();
            Map(x => x.Estado).Column("tx_estado");
            Map(x => x.FechaModificacion).Column("fc_modificacion");
            Map(x => x.IdUsuario).Column("id_usuario");

            Map(x => x.Descripcion).Column("tx_descripcion");
        }
    }
}
