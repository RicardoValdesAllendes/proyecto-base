﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface IRepository<T> : IRORepository<T> where T : class
    {
        bool Create(T entity);
        bool Create(IEnumerable<T> items);
        bool Update(T entity);
        bool Update(IEnumerable<T> items);
        bool Delete(T entity);
        bool Delete(IEnumerable<T> entities);
        bool Save(T entity);
        bool Save(IEnumerable<T> items);
    }
}
