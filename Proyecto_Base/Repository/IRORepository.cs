﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface IRORepository<T> where T:class
    {
        IQueryable<T> GetAll();
        T GetById(int id);
        IQueryable<T> FilterBy(Expression<Func<T, bool>> expression);
    }
}
