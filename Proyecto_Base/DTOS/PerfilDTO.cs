﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOS
{
    public class PerfilDTO : BaseDTO
    {
        public virtual string Descripcion { get; set; }
    }
}
